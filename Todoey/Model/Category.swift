//
//  Category.swift
//  Todoey
//
//  Created by Christian Varriale on 13/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object{
    @objc dynamic var name: String = ""
    @objc dynamic var colorCell: String = ""
    
    let items = List<Item>()    //to define the relationship (Ogni categoria ha più items)
}
