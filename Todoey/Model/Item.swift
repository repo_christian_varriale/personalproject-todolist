//
//  Item.swift
//  Todoey
//
//  Created by Christian Varriale on 13/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object{
    @objc dynamic var title: String = ""
    @objc dynamic var done: Bool = false
    @objc dynamic var dateCreated: Date?
    
    //relazione one-to-one (iberse relationship)
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
