//
//  ViewController.swift
//  Todoey
//
//  Created by Christian Varriale on 11/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework

class TodoListViewController: SwipeTableViewController, UISearchBarDelegate {
    
    //MARK: - Properties
    var todoItems: Results<Item>?
    let realm = try! Realm()
    
    var selectedCategory: Category?{
        didSet{
            loadItems()
        }
    }
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = 80.0
        tableView.separatorStyle = .none
        
        searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        title = selectedCategory!.name
        
        guard let colourHex = selectedCategory?.colorCell else { fatalError("Color Error SelectedCategory")}
        
        updateNavBar(withHexCode: colourHex)
    }
    
    override func viewWillDisappear(_ animated: Bool) {

        updateNavBar(withHexCode: FlatBlue().hexValue())
    }
    
    //MARK: - Nav Bar Setup Methods
    
    func updateNavBar(withHexCode colourHexCode: String){
        
        //Guard Let -> Se c'è la possibilità di avere il fallimento della condizione
        //If let -> 99% di successo
        
        guard let navBar = navigationController?.navigationBar else { fatalError("Nav bar does not exist.")}
        
        guard let navBarColour = UIColor(hexString: colourHexCode) else{ fatalError("Color error Nav Bar") }
        
        navBar.barTintColor = navBarColour
        
        navBar.tintColor = ContrastColorOf(navBarColour, returnFlat: true)
        
        navBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: ContrastColorOf(navBarColour, returnFlat: true)]
        
        searchBar.barTintColor = navBarColour
    }
    
    //MARK: - IBOutlet
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    //MARK: - IBAction
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add New Todoey Item", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Item", style: .default) { (action) in
            
            //what will happen once the user clicks the Add button on our UIAlert
            
            if let currentCategory = self.selectedCategory{
                
                do {
                    
                    try self.realm.write{
                        
                        let newItem = Item()
                        
                        newItem.title = textField.text!
                        newItem.dateCreated = Date()
                        
                        //I have to append this new item to that particular (current) category
                        currentCategory.items.append(newItem)
                        
                    }
                    
                } catch {
                    print("Errore nell'inserimento degli Item, \(error)")
                }
            }
            
            self.tableView.reloadData()
            
        }
        
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Create New Item"
            textField = alertTextField
        }
        
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - TableView Datasource Methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoItems?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        if let item = todoItems?[indexPath.row]{
            
            cell.textLabel?.text = item.title
            
            //ternary Operator -> value = condition ? valueIfTrue : valueIfFalse
            
            cell.accessoryType = item.done == true ? .checkmark : .none
            
            //            if let colour = FlatWhite().darken(byPercentage: CGFloat(indexPath.row) / CGFloat(todoItems!.count) ){
            //
            //                cell.backgroundColor = colour
            //
            //                cell.textLabel?.textColor = ContrastColorOf(colour, returnFlat: true)
            //
            //            }
            
            if let colour = UIColor(hexString: selectedCategory!.colorCell)?.darken(byPercentage: CGFloat(indexPath.row) / CGFloat(todoItems!.count) ){
                
                cell.backgroundColor = colour
                
                cell.textLabel?.textColor = ContrastColorOf(colour, returnFlat: true)
                
            }
            
        }else{
            cell.textLabel?.text = "No Items Added"
        }
        
        return cell
    }
    
    //MARK: - TableView Delegate Methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let item = todoItems?[indexPath.row]{
            
            do {
                try realm.write(){
                    //per cancellare
                    //realm.delete(item)
                    
                    item.done = !item.done
                }
            } catch {
                print("Errore nel salvataggio del done, \(error)")
            }
            
        }
        
        tableView.reloadData()
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func updateModel(at indexPath: IndexPath) {
        
        super.updateModel(at: indexPath)
        
        if let item = todoItems?[indexPath.row]{
            
            do {
                try self.realm.write(){
                    //per cancellare
                    self.realm.delete(item)
                }
            } catch {
                print("Errore nella rimozione della categoria, \(error)")
            }
        }
    }
    
    //MARK: - Function SaveItems & Load Items
    
    // Se il loadItems non prende parametri allora gli passo il NIL, che vuol dire carica tutti quelli che hai salavto
    // Quando richiamata nella searchBar carica i dati che rispettano i due predicati ParentCategory e Title (Situazione del salvataggio nel Context del CoreData)
    
    func loadItems(){
        
        todoItems = selectedCategory?.items.sorted(byKeyPath: "title", ascending: true)
        
        tableView.reloadData()
    }
    
    //MARK: - SearchBar delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        todoItems = todoItems?.filter("title CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "dateCreated", ascending: true)
        //non vanno ricaricati
        tableView.reloadData()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            
            loadItems()
            
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
}

