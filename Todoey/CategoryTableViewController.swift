//
//  CategoryTableViewController.swift
//  Todoey
//
//  Created by Christian Varriale on 12/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework

class CategoryTableViewController: SwipeTableViewController {
    
    //MARK: - Properties
    let realm = try! Realm()
    
    var categories: Results<Category>?
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = 80.0
        
        tableView.separatorStyle = .none
        
        navigationController?.navigationBar.barTintColor = FlatBlue()
        navigationController?.navigationBar.tintColor = FlatWhite()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: FlatWhite()]
        
        loadCategories()
        
    }
    
    // MARK: - Table View Data Source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
//        if let category = categories?[indexPath.row]{
//
//            cell.textLabel?.text = category.name
//
//            cell.backgroundColor = UIColor(hexString: category.colorCell)
//
//        }else{
//
//            cell.textLabel?.text = "No Categories Added Yet"
//
//            cell.backgroundColor = UIColor(hexString: "1D9BF6")
//        }
        
        cell.textLabel?.text = categories?[indexPath.row].name ?? "No Categories Added Yet"
        
        cell.backgroundColor = UIColor(hexString: categories?[indexPath.row].colorCell ?? "1D9BF6")
        
        cell.textLabel?.textColor = ContrastColorOf(UIColor(hexString: (categories?[indexPath.row].colorCell)!)!, returnFlat: true)
        
//        if let colour = FlatPowderBlue().darken(byPercentage: CGFloat(indexPath.row) / CGFloat(categories!.count) ){
//
//            cell.backgroundColor = colour
//
//            cell.textLabel?.textColor = ContrastColorOf(colour, returnFlat: true)
//
//        }
        
        return cell
    }
    
    //MARK: - Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToItems", sender: self)
    }
    
    //MARK: - PrepareForSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinastionVC = segue.destination as! TodoListViewController
        
        if let indexPath = tableView.indexPathForSelectedRow{
            destinastionVC.selectedCategory = categories?[indexPath.row]
        }
    }
    
    //MARK: - Data Manipulation Save & Load
    func save(category: Category){
        
        do {
            try realm.write{
                realm.add(category)
            }
        } catch {
            print("Errore nel salvataggio, \(error)")
        }
        
        tableView.reloadData()
    }
    
    func loadCategories(){
        
        categories = realm.objects(Category.self)
        
        tableView.reloadData()
    }
    
    //MARK: - Delete Data From Swipe
    override func updateModel(at indexPath: IndexPath) {
        
        super.updateModel(at: indexPath)
        
        if let categoryForDeletion = self.categories?[indexPath.row]{
            
            do {
                try self.realm.write(){
                    //per cancellare
                    self.realm.delete(categoryForDeletion)
                }
            } catch {
                print("Errore nella rimozione della categoria, \(error)")
            }
        }
    }
    
    //MARK: - IBAction
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add New Category", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add", style: .default) { (action) in
            
            let newCategory = Category()
            
            newCategory.name = textField.text!
            newCategory.colorCell = UIColor.randomFlat().hexValue()
            
            self.save(category: newCategory)
        }
        
        alert.addTextField { (alertTextField) in
            textField = alertTextField
            alertTextField.placeholder = "Create New Category"
        }
        
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
